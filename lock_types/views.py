from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import LockType
from .serializers import LockTypeSerializer


class ViewLockType(generics.RetrieveDestroyAPIView):
    """ View LockType Details
    """
    queryset = LockType.objects.all()
    serializer_class = LockTypeSerializer


class EditLockType(generics.RetrieveUpdateAPIView):
    """ Edit LockType
    """
    queryset = LockType.objects.all()
    serializer_class = LockTypeSerializer


class ListLockTypes(generics.ListAPIView):
    """ List All Active LockTypes
    """
    queryset = LockType.objects.all()
    serializer_class = LockTypeSerializer


class AddLockType(generics.CreateAPIView):
    """Add a new LockType
    """
    queryset = LockType.objects.all()
    serializer_class = LockTypeSerializer

