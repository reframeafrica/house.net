from django.apps import AppConfig


class LockTypesConfig(AppConfig):
    name = 'lock_types'
