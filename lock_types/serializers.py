from .models import LockType
from rest_framework import serializers



class LockTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = LockType
        fields = '__all__'
