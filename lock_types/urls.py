from django.urls import path
from . import views


app_name = 'lock_types'
urlpatterns = [
    path('list', views.ListLockTypes.as_view(), name='lock_types_list'),
    path('add', views.AddLockType.as_view(), name='lock_types_add'),
    path('<int:pk>', views.ViewLockType.as_view(), name='lock_type_detail'),
    path('<int:pk>/edit', views.EditLockType.as_view(), name='lock_type_edit'),
]
