from django.urls import path
from . import views


app_name = 'unit_types'
urlpatterns = [
    path('list', views.ListUnitTypes.as_view(), name='unit_types_list'),
    path('add', views.AddUnitType.as_view(), name='unit_types_add'),
    path('<int:pk>', views.ViewUnitType.as_view(), name='unit_type_detail'),
    path('<int:pk>/edit', views.EditUnitType.as_view(), name='unit_type_edit'),
]
