from django.apps import AppConfig


class UnitTypesConfig(AppConfig):
    name = 'unit_types'
