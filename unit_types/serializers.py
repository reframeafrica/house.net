from .models import UnitType
from rest_framework import serializers



class UnitTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = UnitType
        fields = '__all__'
