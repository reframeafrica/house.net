from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import UnitType
from .serializers import UnitTypeSerializer


class ViewUnitType(generics.RetrieveDestroyAPIView):
    """ View UnitType Details
    """
    queryset = UnitType.objects.all()
    serializer_class = UnitTypeSerializer


class EditUnitType(generics.RetrieveUpdateAPIView):
    """ Edit UnitType
    """
    queryset = UnitType.objects.all()
    serializer_class = UnitTypeSerializer


class ListUnitTypes(generics.ListAPIView):
    """ List All Active UnitTypes
    """
    queryset = UnitType.objects.all()
    serializer_class = UnitTypeSerializer


class AddUnitType(generics.CreateAPIView):
    """Add a new UnitType
    """
    queryset = UnitType.objects.all()
    serializer_class = UnitTypeSerializer

