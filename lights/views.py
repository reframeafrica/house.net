from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import Light
from .serializers import LightSerializer


class ViewLight(generics.RetrieveDestroyAPIView):
    """ View Light Details
    """
    queryset = Light.objects.all()
    serializer_class = LightSerializer


class EditLight(generics.RetrieveUpdateAPIView):
    """ Edit Light
    """
    queryset = Light.objects.all()
    serializer_class = LightSerializer


class ListLights(generics.ListAPIView):
    """ List All Active Lights
    """
    queryset = Light.objects.all()
    serializer_class = LightSerializer


class AddLight(generics.CreateAPIView):
    """Add a new Light
    """
    queryset = Light.objects.all()
    serializer_class = LightSerializer

