from .models import Light
from rest_framework import serializers



class LightSerializer(serializers.ModelSerializer):

    class Meta:
        model = Light
        fields = '__all__'
