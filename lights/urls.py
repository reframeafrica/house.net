from django.urls import path
from . import views


app_name = 'lights'
urlpatterns = [
    path('list', views.ListLights.as_view(), name='lights_list'),
    path('add', views.AddLight.as_view(), name='lights_add'),
    path('<int:pk>', views.ViewLight.as_view(), name='light_detail'),
    path('<int:pk>/edit', views.EditLight.as_view(), name='light_edit'),
]
