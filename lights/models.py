from django.db import models
from common.models import Entity
from bridges.models import Bridge


class Light(Entity):
    bridge = models.ForeignKey(Bridge, on_delete=models.CASCADE, blank=True, null=True)
    state = models.IntegerField()
    brightness = models.IntegerField()
    channel = models.IntegerField()
     