from django.db import models
from common.models import Entity
from doors.models import Door
from units.models import Unit
from lock_types.models import LockType



class DoorLock(Entity):
    channel = models.IntegerField()
    state = models.IntegerField()
    lock_type = models.ForeignKey(LockType, on_delete=CASCADE, blank=True, null=True)
    door = models.ForeignKey(Door, on_delete=CASCADE, blank=True, null=True)
    
