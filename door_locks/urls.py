from django.urls import path
from . import views


app_name = 'door_locks'
urlpatterns = [
    path('list', views.ListDoorLocks.as_view(), name='door_locks_list'),
    path('add', views.AddDoorLock.as_view(), name='door_locks_add'),
    path('<int:pk>', views.ViewDoorLock.as_view(), name='door_lock_detail'),
    path('<int:pk>/edit', views.EditDoorLock.as_view(), name='door_lock_edit'),
]
