from .models import DoorLock
from rest_framework import serializers



class DoorLockSerializer(serializers.ModelSerializer):

    class Meta:
        model = DoorLock
        fields = '__all__'
