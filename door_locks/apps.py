from django.apps import AppConfig


class DoorLocksConfig(AppConfig):
    name = 'door_locks'
