from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import DoorLock
from .serializers import DoorLockSerializer


class ViewDoorLock(generics.RetrieveDestroyAPIView):
    """ View DoorLock Details
    """
    queryset = DoorLock.objects.all()
    serializer_class = DoorLockSerializer


class EditDoorLock(generics.RetrieveUpdateAPIView):
    """ Edit DoorLock
    """
    queryset = DoorLock.objects.all()
    serializer_class = DoorLockSerializer


class ListDoorLocks(generics.ListAPIView):
    """ List All Active DoorLocks
    """
    queryset = DoorLock.objects.all()
    serializer_class = DoorLockSerializer


class AddDoorLock(generics.CreateAPIView):
    """Add a new DoorLock
    """
    queryset = DoorLock.objects.all()
    serializer_class = DoorLockSerializer

