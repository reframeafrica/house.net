from rest_framework import serializers
from .models import LightType


class LightTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LightType
        fields = '__all__'

    