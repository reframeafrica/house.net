from django.apps import AppConfig


class LightTypesConfig(AppConfig):
    name = 'light_types'
