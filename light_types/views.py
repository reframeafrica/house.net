from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import LightType
from .serializers import LightTypeSerializer


class ViewLightType(generics.RetrieveDestroyAPIView):
    """ View LightType Details
    """
    queryset = LightType.objects.all()
    serializer_class = LightTypeSerializer


class EditLightType(generics.RetrieveUpdateAPIView):
    """ Edit LightType
    """
    queryset = LightType.objects.all()
    serializer_class = LightTypeSerializer


class ListLightTypes(generics.ListAPIView):
    """ List All Active LightTypes
    """
    queryset = LightType.objects.all()
    serializer_class = LightTypeSerializer


class AddLightType(generics.CreateAPIView):
    """Add a new LightType
    """
    queryset = LightType.objects.all()
    serializer_class = LightTypeSerializer



