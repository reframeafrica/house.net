from django.utils import timezone
from django.test import TestCase
from .models import LightType
from common.tests import *

class LightTypeTest(GenericTest, TestCase):
    
    light_type = LightType()

    def setUp(self):
        return self.light_type

class LightTypeListCountTests(LightTypeTest):
    def test_light_type_list(self):
        pass

class LightTypeCreateTestCase(EntityCreateTestCase, LightTypeTest):
    def test_light_type_create(self):
        self.light_type = LightType.objects.create(name="getsi")
        super().test_create_entity(name="getsi")
        
class LightTypeListTestCase(EntityListTestCase, LightTypeTest):
    def test_light_type_list(self):
        self.light_types = LightType.objects.all()
        super().test_list_entities(entity="light_types")

class LightTypeRemoveTestCase(LightTypeTest):
    def test_light_type_remove(self):
        pass


