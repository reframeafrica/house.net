from django.urls import path
from . import views


app_name = 'light_types'
urlpatterns = [
    path('list', views.ListLightTypes.as_view(), name='light_types_list'),
    path('add', views.AddLightType.as_view(), name='light_types_add'),
    path('<int:pk>', views.ViewLightType.as_view(), name='light_type_detail'),
    path('<int:pk>/edit', views.EditLightType.as_view(), name='light_type_edit'),
]
