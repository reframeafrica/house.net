from django.urls import path
from . import views


app_name = 'users'
urlpatterns = [
    path('list', views.ListUsers.as_view(), name='users_list'),
    path('add', views.AddUser.as_view(), name='users_add'),
    path('<int:pk>', views.ViewUser.as_view(), name='user_detail'),
    path('<int:pk>/edit', views.EditUser.as_view(), name='user_edit'),
]
