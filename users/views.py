from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import User
from .serializers import UserSerializer


class ViewUser(generics.RetrieveDestroyAPIView):
    """ View User Details
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class EditUser(generics.RetrieveUpdateAPIView):
    """ Edit User
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ListUsers(generics.ListAPIView):
    """ List All Active Users
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AddUser(generics.CreateAPIView):
    """Add a new User
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer

