from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import Bridge
from .serializers import BridgeSerializer


class ViewBridge(generics.RetrieveDestroyAPIView):
    """ View Bridge Details
    """
    queryset = Bridge.objects.all()
    serializer_class = BridgeSerializer


class EditBridge(generics.RetrieveUpdateAPIView):
    """ Edit Bridge
    """
    queryset = Bridge.objects.all()
    serializer_class = BridgeSerializer


class ListBridges(generics.ListAPIView):
    """ List All Active Bridges
    """
    queryset = Bridge.objects.all()
    serializer_class = BridgeSerializer


class AddBridge(generics.CreateAPIView):
    """Add a new Bridge
    """
    queryset = Bridge.objects.all()
    serializer_class = BridgeSerializer

