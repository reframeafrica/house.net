from .models import Bridge
from rest_framework import serializers



class BridgeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Bridge
        fields = '__all__'
