from django.urls import path
from . import views


app_name = 'bridges'
urlpatterns = [
    path('list/', views.ListBridges.as_view(), name='bridges_list'),
    path('add/', views.AddBridge.as_view(), name='bridges_add'),
    path('<int:pk>', views.ViewBridge.as_view(), name='bridge_detail'),
    path('<int:pk>/edit', views.EditBridge.as_view(), name='bridge_edit'),
    # path('<int:pk>/comments/add', views.AddComment.as_view(), name='bridge_comments_add')
]
