from django.db import models
from common.models import Entity


class Bridge(Entity):
    host = models.CharField(max_length=45)
    port = models.IntegerField()
    