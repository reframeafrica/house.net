from django.db import models
from common.models import Entity
from bridges.models import Bridge
from units.models import Unit


class Geyser(Entity):
    channel = models.IntegerField()
    state = models.IntegerField()
    bridge = models.ForeignKey(Bridge, on_delete=models.CASCADE, blank=True, null=True)

