from django.urls import path
from . import views


app_name = 'geysers'
urlpatterns = [
    path('list', views.ListGeysers.as_view(), name='geysers_list'),
    path('add', views.AddGeyser.as_view(), name='geysers_add'),
    path('<int:pk>', views.ViewGeyser.as_view(), name='geyser_detail'),
    path('<int:pk>/edit', views.EditGeyser.as_view(), name='geyser_edit'),
]
