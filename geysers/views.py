from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import Geyser
from .serializers import GeyserSerializer


class ViewGeyser(generics.RetrieveDestroyAPIView):
    """ View Geyser Details
    """
    queryset = Geyser.objects.all()
    serializer_class = GeyserSerializer


class EditGeyser(generics.RetrieveUpdateAPIView):
    """ Edit Geyser
    """
    queryset = Geyser.objects.all()
    serializer_class = GeyserSerializer


class ListGeysers(generics.ListAPIView):
    """ List All Active Geysers
    """
    queryset = Geyser.objects.all()
    serializer_class = GeyserSerializer


class AddGeyser(generics.CreateAPIView):
    """Add a new Geyser
    """
    queryset = Geyser.objects.all()
    serializer_class = GeyserSerializer

