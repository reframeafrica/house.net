from .models import Geyser
from rest_framework import serializers



class GeyserSerializer(serializers.ModelSerializer):

    class Meta:
        model = Geyser
        fields = '__all__'
