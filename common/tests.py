from django.test import TestCase
from .models import Entity
from typing import TypeVar, Generic
from django.db import models
from users.models import User



class GenericTest(object):

    def setUp(self):
        self.create_user_and_login(self)

    def create_user_and_login(self):
        self.user = User.objects.create(email='uday@email.com')
        self.user.set_password('uday2293')
        self.user.save()
        self.client.login(email='uday@email.com', password='uday2293')


class EntityCountTestCase(GenericTest):
    def test_count_entity(self, **kwarg):
        pass


class EntityCreateTestCase(GenericTest):
    def test_create_entity(self, **kwarg):
        self.entity = self.__class__.setUp(self)
        self.assertEqual(self.entity.name, kwarg.get('name'))
        self.assertNotEqual(self.entity.created_at, None)
        
        
class EntityListTestCase(GenericTest):
    def test_list_entities(self, **kwarg):
        # self.entity = self.__class__.list(self, **kwarg)
        response = self.client.get(f'{{kwarg.get("entity")}}/list/')
        self.assertEqual(response.status_code, 200)


    def test_touch(self):
        self.entity = self.__class__.setUp(self)
        self.create_user_and_login()
        self.entity.touch(self.user)
        self.assertNotEqual(self.entity.created_at, self.entity.updated_at)
        


    