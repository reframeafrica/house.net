from django.db import models
from django.utils import timezone
from house_net import settings

class Entity(models.Model):
    class Meta:
        abstract = True

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, null=True, blank=True)
    created_at = models.DateTimeField(default=timezone.now, null=True)
    updated_at = models.DateTimeField(default=timezone.now, null=True)
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, related_name="+", on_delete=models.CASCADE,)

    class Meta:
        abstract = True

    def touch(self, user, commit=True):
        self.updated_at = timezone.now()
        self.updated_by = user
        if commit:
            self.save