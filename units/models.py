from django.db import models
from common.models import Entity
from unit_types.models import UnitType

class Unit(Entity):
    alias = models.CharField(max_length=45)
    type_id = models.ForeignKey(UnitType, on_delete=models.CASCADE, blank=True, null=True)
    
