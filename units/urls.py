from django.urls import path
from . import views


app_name = 'units'
urlpatterns = [
    path('list', views.ListUnits.as_view(), name='units_list'),
    path('add', views.AddUnit.as_view(), name='units_add'),
    path('<int:pk>', views.ViewUnit.as_view(), name='unit_detail'),
    path('<int:pk>/edit', views.EditUnit.as_view(), name='unit_edit'),
]
