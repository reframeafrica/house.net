from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import Unit
from .serializers import UnitSerializer


class ViewUnit(generics.RetrieveDestroyAPIView):
    """ View Unit Details
    """
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer


class EditUnit(generics.RetrieveUpdateAPIView):
    """ Edit Unit
    """
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer


class ListUnits(generics.ListAPIView):
    """ List All Active Units
    """
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer


class AddUnit(generics.CreateAPIView):
    """Add a new Unit
    """
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer

