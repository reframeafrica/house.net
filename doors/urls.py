from django.urls import path
from . import views


app_name = 'doors'
urlpatterns = [
    path('list', views.ListDoors.as_view(), name='doors_list'),
    path('add', views.AddDoor.as_view(), name='doors_add'),
    path('<int:pk>', views.ViewDoor.as_view(), name='door_detail'),
    path('<int:pk>/edit', views.EditDoor.as_view(), name='door_edit'),
]
