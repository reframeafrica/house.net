from django.db import models
from common.models import Entity
from units.models import Unit
from bridges.models import Bridge


class Door(Entity):
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE, blank=True, null=True)
    bridge = models.ForeignKey(Bridge, on_delete=models.CASCADE, blank=True, null=True)
    channel = models.IntegerField()
    

    
