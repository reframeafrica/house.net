from .models import Door
from rest_framework import serializers



class DoorSerializer(serializers.ModelSerializer):

    class Meta:
        model = Door
        fields = '__all__'
