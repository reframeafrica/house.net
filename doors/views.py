from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from .models import Door
from .serializers import DoorSerializer


class ViewDoor(generics.RetrieveDestroyAPIView):
    """ View Door Details
    """
    queryset = Door.objects.all()
    serializer_class = DoorSerializer


class EditDoor(generics.RetrieveUpdateAPIView):
    """ Edit Door
    """
    queryset = Door.objects.all()
    serializer_class = DoorSerializer


class ListDoors(generics.ListAPIView):
    """ List All Active Doors
    """
    queryset = Door.objects.all()
    serializer_class = DoorSerializer


class AddDoor(generics.CreateAPIView):
    """Add a new Door
    """
    queryset = Door.objects.all()
    serializer_class = DoorSerializer

